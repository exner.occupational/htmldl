# -*- coding: utf-8 -*-

from sys import stdout, stderr
from bs4 import BeautifulSoup
from re import finditer
from enhancements.modules import Module

class AlexBaseModule(Module):

    def execute(self, data):
        pass



class TextifyModule(AlexBaseModule):

    @classmethod
    def parser_arguments(cls):
        cls.PARSER.add_argument(
            '--parser',
            dest='parser',
            type=str,
            default="html.parser",
            help='BeautifulSoup 4 parser'
        )

    def execute(self, data) -> str:
        return BeautifulSoup(data, self.args.parser).text



class CapitalizationModule(AlexBaseModule):

    @classmethod
    def parser_arguments(cls):
        cls.PARSER.add_argument(
            '--mode',
            dest='mode',
            choices=['lower', 'upper'],
            help='Capitalization mode'
        )

    def execute(self, data) -> str:
        if self.args.mode == 'lower':
            return data.lower()
        else:
            return data.upper()



class StreamOutputModule(AlexBaseModule):

    @classmethod
    def parser_arguments(cls):
        cls.PARSER.add_argument(
            '--stream',
            dest='stream',
            choices=['stdout', 'stderr'],
            help='Output stream'
        )

    def execute(self, data) -> str:
        d = { "stdout": stdout, "stderr": stderr}
        print(data, file=d[self.args.stream])
        return data # to be able to write to file after stream output and vice versa



class FileOutputModule(AlexBaseModule):

    @classmethod
    def parser_arguments(cls):
        cls.PARSER.add_argument(
            '--fname',
            dest='fname',
            type=str,
            help='Output filename'
        )

    def execute(self, data) -> str:
        with open(self.args.fname, "w") as of:
            of.write(data)
        return data # to be able to write to file after stream output and vice versa



class LenFilterModule(AlexBaseModule):

    @classmethod
    def parser_arguments(cls):
        cls.PARSER.add_argument(
            '--minlen',
            dest='minlen',
            type=int,
            help='Minimum word length'
        )

    def execute(self, data) -> str:
        return " ".join([x.group() for x in finditer(r'[^\s]+', data) if len(x.group()) > self.args.minlen])

