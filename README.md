# HTMLDL

## Requirements
pip3 install enhancements

## Examples
### Removal of HTML tags, no output
python3 htmldl.py -m htmleditmodules.TextifyModule http://www.orf.at
### Removal of HTML tags using lxml parser, no output
python3 htmldl.py -m htmleditmodules.TextifyModule --parser lxml http://www.orf.at
### Remove HTML tags, write to stderr
python3 htmldl.py -m htmleditmodules.TextifyModule --parser lxml -m htmleditmodules.StreamOutputModule --stream stderr http://www.orf.at
### Remove HTML tags, transform text to uppercase, output to stdout
python3 htmldl.py -m htmleditmodules.TextifyModule -m htmleditmodules.CapitalizationModule --mode upper -m htmleditmodules.StreamOutputModule --stream stdout http://www.orf.at
### Remove HTML tags, transform text to uppercase, output to stdout, output to file
python3 htmldl.py -m htmleditmodules.TextifyModule -m htmleditmodules.CapitalizationModule --mode upper -m htmleditmodules.StreamOutputModule --stream stdout -m htmleditmodules.FileOutputModule --fname ./blah.txt http://www.orf.at
### Remove HTML tags, output to file, transform text to uppercase, output to stdout
python3 htmldl.py -m htmleditmodules.TextifyModule -m htmleditmodules.FileOutputModule --fname ./blah.tx -m htmleditmodules.CapitalizationModule --mode upper -m htmleditmodules.StreamOutputModule --stream stdout http://www.orf.at
### Remove HTML tags, filter by word length, output to stdout
python3 htmldl.py -m htmleditmodules.TextifyModule -m htmleditmodules.LenFilterModule --minlen 10 -m htmleditmodules.StreamOutputModule --stream stdout http://www.orf.at
