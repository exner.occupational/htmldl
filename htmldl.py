# -*- coding: utf-8 -*-

import requests
from enhancements.modules import ModuleParser
from htmleditmodules import *

parser = ModuleParser(baseclass=AlexBaseModule, description='HTML Editing Examples')

parser.add_argument(
    'url',
    action='store',
    help='URL to open'
)

args = parser.parse_args()
modules = [module() for module in args.modules]

response = requests.get(args.url)
content = response.content

print("Status: {}".format(response.status_code))
print("Content length: {}".format(len(content)))

for module in modules:
    a = module.execute.__annotations__
    if "return" in a and a["return"] == type(str()):
        content = module.execute(content)
    else:
        module.execute(content)
